﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneControlLevel1 : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Spawn the flowers and keep track of time upon "Start" click. */

	public GameObject[] flowers; 		// Flowers to be spawned across the screen

	public Text timeText;				// Reference to text keeping track of time
	public GameObject startButton;		// Reference to 'Start' button

	private float maxWidth;				// Maximum width that flowers can be spawned between
	private float maxHeight;			// Highest point flowers can be spawned
	private float minHeight;			// Lowest point flowers can be spawned
	private float timeLeft;				// Amount of time left in game level
	private float clockSpeed;			// Speed of game clock

	void Start () {
		// Set x- and y-axis limits for flowers
		maxWidth = 5.0f;
		maxHeight = 3.0f;
		minHeight = -2.0f;

		// Set game clock
		timeLeft = 20f;
		clockSpeed = 1f;
		timeText.text = "Time: " + timeLeft;

		// Set the panel to Active
		GameControl.control.panel.SetActive(true);
	}

	// Deactivate the panel and start the level
	public void OnClickStart() {
		// Deactivate Start button and Panel
		startButton.SetActive (false);
		GameControl.control.panel.SetActive (false);

		// Spawn the objects and start the clock
		Spawn ();
		InvokeRepeating ("Clock", 0, clockSpeed);
	}

	// If the peanut man falls out of the level... End the current level
	void OnTriggerEnter2D(Collider2D other) {
		timeLeft = 1;
	}

	// Set the game clock and visible text
	void Clock() {
		timeLeft--;
		timeText.text = ("Time: " + timeLeft);
		if (timeLeft <= 0) {
			// Set the new high scores and load the next scene
			GameControl.control.SetHighScoreList();
			Application.LoadLevel (SceneManager.GetActiveScene().buildIndex + 1);
		}
	}

	// Spawn the flowers at every point (i, j)
	void Spawn() {
		for (float i = -maxWidth; i <= maxWidth; i++) {
			for (float j = minHeight; j <= maxHeight; j++) {
				// Get a flower at random
				GameObject flower = flowers [Random.Range (0, flowers.Length)];
				// Set the new position and rotation
				Vector3 spawnPosition = new Vector3((flower.CompareTag("Purple") ? (i - 0.25f) : i ), j);
				Quaternion spawnRotation = new Quaternion (-90.0f, 0.0f, 0.0f, 90.0f);
				// Set the flower to visible and instantiate the object
				flower.SetActive(true);
				Instantiate (flower, spawnPosition, spawnRotation);
			}
		}
	}
}
