﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {

	/* GOAL OF THIS CLASS: 
	 * Take care of any INFORMATION pertaining to the game. 
	 * Do not touch any of the UI except for the panel (to activate/deactivate). */

	public static GameControl control;		// Reference to this script

	public GameObject panel;				// Reference to panel
	public bool gameIsOver;					// Determines if 'Save & Exit' is called at the end of the game,
											// Sets the scene to be loaded index to 0

	private float count;					// Current flower count
	private float[] scoreList = new float[] {0f, 0f, 0f};		// List of high scores

	private string[] levelDescriptions = { "Level 1\nCollect as many flowers as you can before the time runs out!", 
		"Level 2\nWatch out for the asteroids! They'll knock you back 2 points for every hit.",
		"Level 3\nMore asteroids and less time. Good luck!"};			// Descriptions for each level
	
	private string savedGameFile = "/savedGame.dat";			// Name of Saved Game file

	// Awake happens before Start
	void Awake() {
		// Singleton GameControl initalization
		if (control == null) {
			DontDestroyOnLoad (gameObject);
			control = this;
		}
		else if (control != this) {
			Destroy (gameObject);
		}
	}

	// Load a previously saved game
	public void OnClickLoad() {
		// Open the BinaryFormatter and FileStream
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Open (Application.persistentDataPath + savedGameFile, FileMode.Open);

		// Gather and load the data
		GameData data = (GameData) bf.Deserialize (file);
		scoreList = data.scores;
		count = data.count;
		Application.LoadLevel (data.sceneIndex);

		// Close and destroy the file
		file.Close ();
		file.Dispose ();
	}

	// Save current game data
	public void OnClickSave() {
		// Open the new BinaryFormatter and FileStream
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + savedGameFile);

		// Initalize the Game Data to be saved
		GameData data;
		if (gameIsOver) {
			data = new GameData (count, 0, scoreList);
			gameIsOver = false;
		}
		else {
			data = new GameData (count, SceneManager.GetActiveScene ().buildIndex, scoreList);
		}

		// Serialize the data and close the file
		bf.Serialize(file, data);
		file.Close();

		// End the game
		Application.Quit ();
	}

	// Check if a saved game exists
	public bool SavedGameExists() {
		if (File.Exists (Application.persistentDataPath + savedGameFile))
			return true;
		return false;
	}

	// Return the current high scores list
	public float[] GetScoreList() {
		return scoreList;
	}

	// Reset the high scores. Calls internal method with proper parameters.
	public void SetHighScoreList() {
		this.SetHighScoreList (count);
	}

	// Reset the high scores. Return true if list has been changed.
	public void SetHighScoreList (float curr) {
		float currentScore = curr;
		// For every element in the list... Check if 'curr' is greater
		for (int i = 0; i < this.scoreList.Length; i++) { 
			// If so... swap the elements and set 'changed' to true
			if (currentScore > scoreList [i]) {
				float temp = scoreList [i];
				scoreList [i] = currentScore;
				currentScore = temp;
			}
		}
	}

	// Get current level description
	public string GetLevelText() {
		int index = SceneManager.GetActiveScene().buildIndex;
		return levelDescriptions[index];
	}

	// Return count text
	public string GetCountText() {
		return (count + " flowers collected");
	}

	// Set the count value
	public void SetCount(float c) {
		count = c;
	}

	// Increment count value
	public void UpdateCount(float c) {
		count += c;
	}
}