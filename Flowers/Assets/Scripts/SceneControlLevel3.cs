﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneControlLevel3 : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Spawn the flowers and asteroids */

	public GameObject[] flowers; 		// Flowers to be spawned across the screen
	public GameObject asteroid;			// Fire game object

	public Text timeText;
	public GameObject startButton;			// Reference to 'Start' button

	private float maxWidth;				// Maximum width that flowers can be spawned between
	private float maxHeight;			// Highest point flowers can be spawned
	private float minHeight;			// Lowest point flowers can be spawned
	private float timeLeft;				// Amount of time left in game level
	private float clockSpeed;			// Speed of game clock
	private bool gameOver = false;		// To know if the game is over

	void Start () {
		// Set x- and y-axis limits for flowers
		maxWidth = 5.0f;
		maxHeight = 3.0f;
		minHeight = -2.0f;

		// Set game clock
		timeLeft = 15f;
		clockSpeed = 1f;
		timeText.text = "Time: " + timeLeft; 

		// Activate the panel
		GameControl.control.panel.SetActive(true);
	}

	// Deactivate the panel and start the level
	public void OnClickStart() {
		// When the game is over... Play again from the beginning!
		if (gameOver) {
			// Reload the first level
			GameControl.control.SetCount(0);
			Application.LoadLevel (0);

			// Reset the gameOver variables since we are replaying the game
			gameOver = false; 
			GameControl.control.gameIsOver = false;
		}
		// Otherwise, start the level!
		else if (!gameOver) {
			// Deactivate the 'Start' button and panel
			startButton.SetActive (false);
			GameControl.control.panel.SetActive (false);

			// Spawn the objects and start the clock
			Spawn ();
			InvokeRepeating ("Clock", 0, clockSpeed);
		}
	}

	// If the peanut man falls out of the level... End the current level
	void OnTriggerEnter2D(Collider2D other) {
		timeLeft = 1;
	}

	// Set the game clock and visible text
	void Clock() {
		timeLeft--;
		timeText.text = ("Time: " + timeLeft);
		// When the game is over...
		if (timeLeft <= 0) {
			// Reset the start button so that it reads 'Play again'
			startButton.GetComponentInChildren<Text> ().text = "Play again?";
			startButton.gameObject.SetActive (true);

			//Set new high scores and Activate the panel
			GameControl.control.SetHighScoreList ();
			GameControl.control.panel.SetActive (true);

			// GameIsOver variable determines what scene index is saved if the user
			// presses 'Save & Exit' at the end of scene 3
			GameControl.control.gameIsOver = true;

			// Set the gameOver variable to true and cancel the InvokeRepeating
			gameOver = true;
			CancelInvoke ();
		}
	}

	// Spawn the flowers at every point (i, j)
	void Spawn() {
		for (float i = -maxWidth; i <= maxWidth; i++) {
			for (float j = minHeight; j <= maxHeight; j++) {
				// Build new game object
				GameObject go = new GameObject ();

				// At specific indices... Get an asteroid. Otherwise, pick a random flower
				if ((Mathf.Abs (i) % 2 == 0) && (Mathf.Abs(j) % 2 == 0))
					go = asteroid;
				else
					go = flowers [Random.Range (0, flowers.Length)];
				
				// Set the position and rotation
				Vector3 spawnPosition = new Vector3((go.CompareTag("Purple") ? (i - 0.25f) : i ), j);
				Quaternion spawnRotation = new Quaternion ((go.CompareTag("Asteroid") ? 0.0f : -90.0f), 0.0f, 0.0f, 90.0f);

				// Instantiate the object
				Instantiate (go, spawnPosition, spawnRotation);
			}
		}
	}
}
