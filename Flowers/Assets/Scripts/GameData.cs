﻿using System;

[Serializable]
public class GameData {

	/* GOAL OF THIS CLASS:
	 * Hold all the data for a given "Game" that can be reloaded at will. 
	 * Note: Does NOT hold current top scores. */

	public float count; 			// Current number of flowers collected
	public int sceneIndex;			// Build index of active scene at time of save
	public float[] scores;			// High scores

	public GameData(float count, int sceneIndex, float[] scores) {
		this.count = count;
		this.sceneIndex = sceneIndex;
		this.scores = scores;
	}
}
