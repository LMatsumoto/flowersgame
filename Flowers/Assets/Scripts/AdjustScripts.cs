﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdjustScripts : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Make sure UI is displaying the correct information.
	 * Should be attached to the Camera on each scene.
	 * Notes: TimeText is handled in individual scenes. 
	 * 		  Panel will be activated/deactivated in individal scenes. */

	public Text flowerText;				// Reference to text holding flower count
	public Text levelText;				// Reference to text describing the level
	public Text[] scoreTexts;			// Reference to score list Text objects
	public Button loadButton;			// Reference to Load Saved Game button
	public Button saveButton;			// Reference to Save Current Game button

	// Update at each frame
	void Update() {
		// print ("AdjustScripts Update");
		// Update the flower count text
		flowerText.text = GameControl.control.GetCountText ();

		// Update level text
		levelText.text = GameControl.control.GetLevelText ();

		// Update the scores
		scoreTexts = GetScoreTexts();

		// Check if LoadButton should be interactable
		loadButton.interactable = GameControl.control.SavedGameExists ();

		// Set the save button to be interactable
		saveButton.interactable = true;
	}

	// Set the high score Text objects
	Text[] GetScoreTexts() {
		// Text[] textList = new Text[3];
		float[] list = new float[scoreTexts.Length];
		list = GameControl.control.GetScoreList();
		float scoreListing = new float ();
		for (int i = 0; i < this.scoreTexts.Length; i++) {
			scoreListing = list [i];
			this.scoreTexts[i].text = scoreListing.ToString();
		}
		return scoreTexts;
	}
}
