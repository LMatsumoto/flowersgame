﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Decrement player points upon hitting an asteroid. */

	void OnTriggerEnter2D(Collider2D other) {
		GameControl.control.UpdateCount (-2.0f);
		Destroy (gameObject);
	}
}
