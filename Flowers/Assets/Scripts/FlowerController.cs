﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerController : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Increment the flower count whenever a flower trigger is entered */

	void OnTriggerEnter2D(Collider2D other) {
		GameControl.control.UpdateCount (1.0f);
		Destroy (gameObject);
	}
}
